﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 错误代码
    /// </summary>
    public enum EZErrorCode : int
    {
        /// <summary>
        /// 请求成功
        /// </summary>
        OK = 200,
        /// <summary>
        /// 用户名不合法
        /// </summary>
        InvalidAppkey = 1001,
        /// <summary>
        /// 用户名已被占用
        /// </summary>
        AppkeyOccupied,
        /// <summary>
        /// 密码不合法
        /// </summary>
        InvalidPassword,
        /// <summary>
        /// 密码为同一字符
        /// </summary>
        PasswordSameChar,
        /// <summary>
        /// 密码错误次数过多
        /// </summary>
        PasswordErrorMoreThanLimit,
        /// <summary>
        /// 手机号码已注册
        /// </summary>
        PhoneNumberHasRegistered,
        /// <summary>
        /// 手机号未注册
        /// </summary>
        PhoneNumberNotRegistered,
        /// <summary>
        /// 手机号码不合法
        /// </summary>
        InvalidPhoneNumber,
        /// <summary>
        /// 用户名与手机不匹配
        /// </summary>
        AppkeyAndPhoneNumberNotMatch,
        /// <summary>
        /// 获取验证码失败
        /// </summary>
        ObtainVerificationCodeFailed,
        /// <summary>
        /// 验证码错误
        /// </summary>
        VerificationCodeError,
        /// <summary>
        /// 验证码失效
        /// </summary>
        VerificationCodeInvalid,
        /// <summary>
        /// 用户不存在
        /// </summary>
        AppkeyNotExist,
        /// <summary>
        /// 密码不正确
        /// </summary>
        PasswordIncorrect,
        /// <summary>
        /// 用户被锁住
        /// </summary>
        AppKeyLocked,
        /// <summary>
        /// 验证参数异常
        /// </summary>
        ValidationParameterException = 1021,
        /// <summary>
        /// 邮箱已经被注册
        /// </summary>
        EmailHasRegistered = 1026,
        /// <summary>
        /// 邮箱未注册
        /// </summary>
        EmailNotRegistered = 1031,
        /// <summary>
        /// 邮箱不合法
        /// </summary>
        EmailInvalid,
        /// <summary>
        /// 获取验证码过于频繁
        /// </summary>
        RequestVerificationCodeMoreThanLimit = 1041,
        /// <summary>
        /// 手机验证码输入错误超过规定次数
        /// </summary>
        VerificationCodeInputErrorMoreThanTime = 1043,
        /// <summary>
        /// 设备不存在
        /// </summary>
        DeviceNotExist = 2000,
        /// <summary>
        /// 摄像机不存在
        /// </summary>
        CameraNotExist,
        /// <summary>
        /// 设备不在线
        /// </summary>
        DeviceOffline = 2003,
        /// <summary>
        /// 设备异常
        /// </summary>
        DeviceAbnormal,
        /// <summary>
        /// 设备序列号不正确
        /// </summary>
        DeviceSerialNotCorrect = 2007,
        /// <summary>
        /// 设备请求响应超时异常
        /// </summary>
        DeviceRequestTime = 2009,
        /// <summary>
        /// 设备不支持
        /// </summary>
        DeviceNotSupport = 2030,
        /// <summary>
        /// 设备已被自己添加
        /// </summary>
        DeviceAddedToOwn = 5000,
        /// <summary>
        /// 设备已被别人添加
        /// </summary>
        DeviceAddedToOther,
        /// <summary>
        /// 设备验证码错误
        /// </summary>
        DeviceVerificationCodeError,
        /// <summary>
        /// 邀请不存在
        /// </summary>
        InvitationNotExist = 7001,
        /// <summary>
        /// 邀请验证失败
        /// </summary>
        InvitationVerificateError,
        /// <summary>
        /// 邀请用户不匹配
        /// </summary>
        InvitationNotMatch,
        /// <summary>
        /// 无法取消邀请
        /// </summary>
        UnableCancelInvitation,
        /// <summary>
        /// 无法删除邀请
        /// </summary>
        UnableDeleteInvitation,
        /// <summary>
        /// 不能邀请自己
        /// </summary>
        UnableInvitationOwn,
        /// <summary>
        /// 重复邀请
        /// </summary>
        DuplicateInvitation,
        /// <summary>
        /// 参数错误
        /// </summary>
        ParameterError = 10001,
        /// <summary>
        /// AccessToken异常或过期
        /// </summary>
        AccessTokenExceptionOrExpiration,
        /// <summary>
        /// 用户不存在
        /// </summary>
        UserNotExist = 10004,
        /// <summary>
        /// appKey异常
        ///     确认appKey状态，不通过或者冻结状态会返回该错误码
        /// </summary>
        AppkeyException,
        /// <summary>
        /// ip受限
        /// </summary>
        IPLimited,
        /// <summary>
        /// 调用次数达到上限
        /// </summary>
        RequestMoreThanLimit,
        /// <summary>
        /// 签名错误
        /// </summary>
        SignError,
        /// <summary>
        /// 签名参数错误
        /// </summary>
        SignParamError,
        /// <summary>
        /// 签名超时(需要同步服务器时间)
        /// </summary>
        SignTimeOut,
        /// <summary>
        /// 未开通萤石云服务
        /// </summary>
        ServiceNotOpen,
        /// <summary>
        /// 第三方账户与萤石账号已经绑定
        /// </summary>
        ThirdPartyAccountHasBound,
        /// <summary>
        /// 应用没有权限调用此接口
        /// </summary>
        NotHasPermission,
        /// <summary>
        /// APPKEY下对应的第三方userId和phone未绑定,
        ///     获取AccessToken时所用appKey与SDK所用appKey不一致
        /// </summary>
        UserIDAndPhoneNotBind,
        /// <summary>
        /// appKey不存在
        /// </summary>
        AppkeyNotExist10017 = 10017,
        /// <summary>
        /// AccessToken与Appkey不匹配
        /// </summary>
        AccessTokenAndAppkeyNotMatch,
        /// <summary>
        /// 密码错误
        /// </summary>
        PasswordError,
        /// <summary>
        /// 请求方法为空
        /// </summary>
        RequestMethodIsEmpty,
        /// <summary>
        /// 通道不存在
        /// </summary>
        CameraNoNotExist = 20001,
        /// <summary>
        /// 设备不存在
        ///     ①设备没有注册到萤石云平台，请检查下设备网络参数，确保能正常连接网络
        ///     ②设备序列号不存在
        /// </summary>
        DeviceNotExist20002,
        /// <summary>
        /// 参数异常，SDK版本过低
        /// </summary>
        SDKVersionTooLow20003,
        /// <summary>
        /// 参数异常，SDK版本过低
        /// </summary>
        SDKVersionTooLow20004,
        /// <summary>
        /// 安全认证失败
        /// </summary>
        AuthorizationFailure,
        /// <summary>
        /// 网络异常
        /// </summary>
        NetException,
        /// <summary>
        /// 设备不在线
        /// </summary>
        DeviceOffline20007,
        /// <summary>
        /// 设备响应超时
        /// </summary>
        DeviceTimeout,
        /// <summary>
        /// 子账号不能添加设备
        /// </summary>
        AccountNotAddDevice,
        /// <summary>
        /// 设备验证码错误
        ///     验证码在设备标签上，六位大写字母，注意大小写
        /// </summary>
        DeviceAuthenticationCodeError,
        /// <summary>
        /// 设备添加失败
        /// </summary>
        DeviceAddFailured= 20012,
        /// <summary>
        /// 设备已被别人添加
        /// </summary>
        DeviceHasAddedToOther,
        /// <summary>
        /// 设备序列号不正确
        /// </summary>
        DeviceSerialNumberIsNotCorrect,
        /// <summary>
        /// 设备不支持该功能
        /// </summary>
        DeviceSerialNotSupportFunction,
        /// <summary>
        /// 当前设备正在格式化
        /// </summary>
        DeviceIsFormating,
        /// <summary>
        /// 设备已被自己添加
        /// </summary>
        DeviceHasAddedToOwn,
        /// <summary>
        /// 该用户不拥有该设备
        ///     确认设备是否属于用户
        /// </summary>
        UserDoesNotOwnTheDevice,
        /// <summary>
        /// 设备不支持云存储服务
        /// </summary>
        DeviceDoesNotSupportCloudStorageService,
        /// <summary>
        /// 设备在线，被自己添加
        /// </summary>
        DeviceOnlineAndAddedToOwn,
        /// <summary>
        /// 设备在线，但是未被用户添加
        /// </summary>
        DeviceOnlineAndNotAdded,
        /// <summary>
        ///设备在线，但是已经被别的用户添加
        /// </summary>
        DeviceOnlineAndAddedToOther,
        /// <summary>
        ///设备不在线，未被用户添加
        /// </summary>
        DeviceOfflineAndNotAdded,
        /// <summary>
        ///设备不在线，但是已经被别的用户添加
        /// </summary>
        DeviceOfflineAndAddedToOther,
        /// <summary>
        ///重复申请分享
        ///     确认设备是否由添加过该设备且申请过分享的账户下是否还存在分享记录
        /// </summary>
        RepeatRequestToShare,
        /// <summary>
        /// 视频广场不存在该视频
        /// </summary>
        VideoPlazaDoesNotExistTheVideo,
        /// <summary>
        /// 设备不在线，但是已经被自己添加
        /// </summary>
        DeviceOfflineAndAddedToOwn= 20029,
        /// <summary>
        /// 该用户不拥有该视频广场视频
        /// </summary>
        UserDoesNotOwnTheVideoPlazaVideo,
        /// <summary>
        /// 开启终端绑定，硬件特征码验证失败
        /// </summary>
        EnabledTerminalBound,
        /// <summary>
        /// 该用户下通道不存在
        /// </summary>
        ChannelDoesNotExistUnderUser,
        /// <summary>
        /// 无法收藏自己分享的视频
        /// </summary>
        CanNotKeepOwnShareOfTheVideo,
        /// <summary>
        /// 视频分享给本人
        /// </summary>
        VideoShareToOwn= 20101,
        /// <summary>
        /// 无相应邀请信息
        /// </summary>
        NoInvitationInformation,
        /// <summary>
        /// 操作报警信息失败
        /// </summary>
        OperationAlarmInformationFailed = 20201,

        /// <summary>
        /// 操作留言信息失败
        /// </summary>
        OperationLeavingInformationFailed = 20201,
        /// <summary>
        /// 根据UUID查询报警消息不存在
        /// </summary>
        UUIDQueryAlarmMessageDoesNotExist = 20301,
        /// <summary>
        /// 根据UUID查询图片不存在 
        /// </summary>
        UUIDQueryPicDoesNotExist,
        /// <summary>
        /// 根据FID查询图片不存在
        /// </summary>
        FIDQueryPicDoesNotExist,
        /// <summary>
        /// 数据异常
        /// </summary>
        DataException = 49999,
        /// <summary>
        /// 服务器异常
        /// </summary>
        ServerException = 50000,
        /// <summary>
        /// 设备不支持云台控制
        /// </summary>
        DeviceNotSupportPTZControl = 60000,
        /// <summary>
        /// 用户无云台控制权限
        /// </summary>
        UserNotHasPTZControlPermission,
        /// <summary>
        /// 设备云台旋转达到上限位
        /// </summary>
        PTZTherotaryReachedTheUpperLimit,
        /// <summary>
        /// 设备云台旋转达到下限位
        /// </summary>
        PTZTherotaryReachedTheLowLimit,
        /// <summary>
        /// 设备云台旋转达到左限位
        /// </summary>
        PTZTherotaryReachedTheRightLimit,
        /// <summary>
        /// 设备云台旋转达到右限位
        /// </summary>
        PTZTherotaryReachedTheLeftLimit,
        /// <summary>
        /// 云台当前操作失败
        /// </summary>
        PTZOperationFailure,
        /// <summary>
        /// 预置点个数超过最大值
        /// </summary>
        NumberOfPresetPointsExceedsTheMaximumValue,
        /// <summary>
        /// C6预置点个数达到上限，无法添加
        /// </summary>
        C6PresetPointsMoreThanLimit,
        /// <summary>
        /// 正在调用预置点
        /// </summary>
        PresetPointsIsCalling,
        /// <summary>
        /// 预置点已经是当前位置
        /// </summary>
        PresetPointsIsCurrentLocation,
        /// <summary>
        /// 预置点不存在
        /// </summary>
        PresetPointIsExist,
        /// <summary>
        /// 未知错误
        /// </summary>
        UnknowError,
        /// <summary>
        /// 设备版本已是最新
        /// </summary>
        DeviceVersionIsRecent,
        /// <summary>
        /// 设备正在升级
        /// </summary>
        DeviceIsUpgrading,
        /// <summary>
        /// 设备正在重启
        /// </summary>
        DeviceIsRestarting,
        /// <summary>
        /// 加密未开启，无须关闭
        /// </summary>
        EncryptionIsNotEnabled,
        /// <summary>
        /// 设备抓图失败
        /// </summary>
        DeviceCaptureFailure,
        /// <summary>
        /// 设备升级失败
        /// </summary>
        DeviceUpgradeFailure,
        /// <summary>
        /// 加密已开启
        /// </summary>
        EncryptionIsEnabled,
        /// <summary>
        /// 不支持该命令
        /// </summary>
        NotSupportCommand,
        /// <summary>
        /// 已是当前布撤防状态
        /// </summary>
        CurrentStateIsDefence,
        /// <summary>
        /// 已是当前状态
        /// </summary>
        IsCurrentState,
    }
}

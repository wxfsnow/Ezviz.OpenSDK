﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    ///  布撤防状态
    /// </summary>
    public enum EZDefenceStatus
    {
        /// <summary>
        /// 撤防
        /// </summary>
        UN_DEFENCE,
        /// <summary>
        /// 布防
        /// </summary>
        DEFENCE,
        /// <summary>
        /// 不支持
        /// </summary>
        UN_SUPPORT,
        /// <summary>
        /// 强制布防，A1设备
        /// </summary>
        FORCE_DEFENCE,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 警情状态
    /// </summary>
    public enum EZAlarmStatus:int
    {
        /// <summary>
        /// 未读
        /// </summary>
        UNREAD = 0,
        /// <summary>
        /// 已读
        /// </summary>
        READ,
        /// <summary>
        /// 所有
        /// </summary>
        ALL
    }
}

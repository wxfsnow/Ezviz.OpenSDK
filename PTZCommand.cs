﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 云台控制命令
    /// </summary>
    public enum PTZCommand : int
    {
        UP = 0,
        DOWN,
        LEFT,
        RIGHT,
        UPLEFT,
        DOWNLEFT,
        UPRIGHT,
        DOWNRIGHT,
        ZOOMIN,
        ZOOMOUT,
        FOCUSNEAR,
        FOCUSFAR,
        IRISSTARTUP,
        IRISSTOPDOWN,
        LIGHT,
        WIPER,
        AUTO
    }
}

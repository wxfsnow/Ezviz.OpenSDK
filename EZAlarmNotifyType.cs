﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 开放SDK告警通知类型定义
    /// </summary>
    public enum EZAlarmNotifyType
    {
        /// <summary>
        /// SDK同萤石平台连接异常
        /// </summary>
        ALARM_NOTIFY_CONNECT_EXCEPTION = 100,
        /// <summary>
        /// 重连成功
        /// </summary>
        ALARM_NOTIFY_RECONNECT_SUCCESS,
        /// <summary>
        /// 重连失败
        /// </summary>
        ALARM_NOTIFY_RECONNECT_FAILED
    }
}

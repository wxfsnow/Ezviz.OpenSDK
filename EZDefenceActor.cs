﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 布防参与者
    /// </summary>
    public enum EZDefenceActor
    {
        /// <summary>
        ///  设备
        /// </summary>
        D,
        /// <summary>
        /// 视频通道
        /// </summary>
        V,
        /// <summary>
        /// IO通道
        /// </summary>
        I,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 设备
    /// </summary>
    public class EZDevice
    {
        [JsonProperty("deviceId")]
        public string DeviceId
        {
            get;
            internal set;
        }

        [JsonProperty("deviceSerial")]
        public string DeviceSerial
        {
            get;
            internal set;
        }

        [JsonProperty("deviceName")]
        public string DeviceName
        {
            get;
            internal set;
        }
        /// <summary>
        /// 设备在线状态, 0-默认值; 1-在线; 2-不在线; 4-异常情况
        /// </summary>
        [JsonProperty("status")]
        public int Status
        {
            get;
            internal set;
        }

        /// <summary>
        /// 监控点图片url
        /// </summary>
        [JsonProperty("picUrl")]
        public string PicUrl
        {
            get;
            internal set;
        }
        /// <summary>
        /// 加密状态
        /// </summary>
        [JsonProperty("isEncrypt")]
        public bool IsEncrypt
        {
            get;
            internal set;
        }

        /// <summary>
        /// 设备布撤防状态
        ///     True=布防,False=未布防
        /// </summary>
        [JsonProperty("defence")]
        public bool Defence
        {
            get;
            internal set;
        }
    }
}

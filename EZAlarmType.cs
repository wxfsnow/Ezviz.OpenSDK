﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 开放SDK告警类型定义
    /// </summary>
    public enum EZAlarmType
    {
        /// <summary>
        /// 人体感应事件
        /// </summary>
        BODY_SENSOR_EVENT = 10000,
        /// <summary>
        /// 紧急遥控按钮事件
        /// </summary>
        EMERGENCY_BUTTON_EVENT = 10001,
        /// <summary>
        ///  移动侦测报警
        /// </summary>
        MOTION_DETECT_ALARM = 10002,
        /// <summary>
        /// 婴儿啼哭报警
        /// </summary>
        BABY_CRY_ALARM = 10003,
        /// <summary>
        /// 门磁报警
        /// </summary>
        MAGNETIC_ALARM = 10004,
        /// <summary>
        ///  烟感报警
        /// </summary>
        SMOKE_DETECTOR_ALARM = 10005,
        /// <summary>
        ///  可燃气体报警
        /// </summary>
        COMBUSTIBLE_GAS_ALARM = 10006,
        /// <summary>
        /// 水浸报警
        /// </summary>
        FLOOD_IN_ALARM = 10008,
        /// <summary>
        ///  紧急按钮报警
        /// </summary>
        EMERGENCY_BUTTON_ALARM = 10009,
        /// <summary>
        /// 人体感应报警
        /// </summary>
        BODY_SENSOR_ALARM = 10010,
        /// <summary>
        ///  遮挡报警
        /// </summary>
        SHELTER_ALARM = 10011,
        /// <summary>
        /// 视频丢失
        /// </summary>
        VIDEO_LOSS_ALARM = 10012,
        /// <summary>
        ///  越界侦测
        /// </summary>
        LINE_DETECTION_ALARM = 10013,
        /// <summary>
        /// 区域入侵
        /// </summary>
        FIELD_DETECTION_ALARM = 10014,
        /// <summary>
        /// 人脸检测事件
        /// </summary>
        FACE_DETECTION_ALARM = 10015,
        /// <summary>
        ///  智能门铃报警
        /// </summary>
        DOOR_BELL_ALARM = 10016,
        /// <summary>
        /// 摄像机失去关联报警
        /// </summary>
        DEVOFFLINE_ALARM = 10017,
        /// <summary>
        ///  幕帘报警
        /// </summary>
        CURTAIN_ALARM = 10018,
        /// <summary>
        /// 单体门磁报警
        /// </summary>
        MOVE_MAGNETOMETER_ALARM = 10019,
        /// <summary>
        /// 场景变更侦测
        /// </summary>
        SCENE_CHANGE_DETECTION_ALARM = 10020,
        /// <summary>
        ///  虚焦侦测
        /// </summary>
        DEFOCUS_ALARM = 10021,
        /// <summary>
        /// 音频异常侦测
        /// </summary>
        AUDIO_EXCEPTION_ALARM = 10022,
        /// <summary>
        ///  物品遗留侦测
        /// </summary>
        LEFT_DETECTION_ALARM = 10023,
        /// <summary>
        /// 物品拿取侦测
        /// </summary>
        TAKE_DETECTION_ALARM = 10024,
        /// <summary>
        /// 非法停车侦测
        /// </summary>
        PARKING_DETECTION_ALARM = 10025,
        /// <summary>
        ///  人员聚集侦测
        /// </summary>
        HIGH_DENSITY_DETECTION_ALARM = 10026,
        /// <summary>
        /// 徘徊检测侦测
        /// </summary>
        LOITER_DETECTION_ALARM = 10027,
        /// <summary>
        /// 快速移动侦测
        /// </summary>
        RUN_DETECTION_ALARM = 10028,
        /// <summary>
        ///  进入区域侦测
        /// </summary>
        ENTER_AREA_DETECTION_ALARM = 10029,
        /// <summary>
        /// 离开区域侦测
        /// </summary>
        EXIT_AREA_DETECTION_ALARM = 10030,
        /// <summary>
        /// 磁干扰报警
        /// </summary>
        MAG_GIM_ALARM = 10031,
        /// <summary>
        /// 电池欠压报警
        /// </summary>
        UNDER_VOLTAGE_ALARM = 10032,
        /// <summary>
        /// 闯入报警
        /// </summary>
        INTRUSION_ALARM = 10033,
        /// <summary>
        /// IO报警
        /// </summary>
        IO_00_ALARM = 10100,
        /// <summary>
        /// IO-1报警
        /// </summary>
        IO_01_ALARM = 10101,
        /// <summary>
        /// IO-2报警
        /// </summary>
        IO_02_ALARM = 10102,
        /// <summary>
        /// IO-3报警
        /// </summary>
        IO_03_ALARM = 10103,
        /// <summary>
        /// IO-4报警
        /// </summary>
        IO_04_ALARM = 10104,
        /// <summary>
        /// IO-5报警
        /// </summary>
        IO_05_ALARM = 10105,
        /// <summary>
        /// IO-6报警
        /// </summary>
        IO_06_ALARM = 10106,
        /// <summary>
        /// IO-7报警
        /// </summary>
        IO_07_ALARM = 10107,
        /// <summary>
        ///  IO-8报警
        /// </summary>
        IO_08_ALARM = 10108,
        /// <summary>
        /// IO-9报警
        /// </summary>
        IO_09_ALARM = 10109,
        /// <summary>
        /// IO-10报警
        /// </summary>
        IO_10_ALARM = 10110,
        /// <summary>
        /// IO-10报警
        /// </summary>
        IO_11_ALARM = 10111,
        /// <summary>
        /// IO-12报警
        /// </summary>
        IO_12_ALARM = 10112,
        /// <summary>
        /// IO-13报警
        /// </summary>
        IO_13_ALARM = 10113,
        /// <summary>
        /// IO-14报警
        /// </summary>
        IO_14_ALARM = 10114,
        /// <summary>
        /// IO-15报警
        /// </summary>
        IO_15_ALARM = 10115,
        /// <summary>
        /// IO-16报警
        /// </summary>
        IO_16_ALARM = 10116,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Ezviz.OpenSDK
{
    public class EZAlarm
    {
        [JsonProperty("alarmID")]
        public string AlarmId
        {
            get;
            set;
        }

        [JsonProperty("alarmTime")]
        public DateTime AlarmTime
        {
            get;
            set;
        }

        [JsonProperty("channelID")]
        public int ChannelID
        {
            get;
            set;
        }
        [JsonProperty("channelName")]
        public string ChannelName
        {
            get;
            set;
        }
        [JsonProperty("content")]
        public string Content
        {
            get;
            set;
        }
        [JsonProperty("alarmType")]
        public EZAlarmType AlarmType
        {
            get; set;
        }

        [JsonProperty("picUrl")]
        public string PicUrl
        {
            get;
            set;
        }

        [JsonProperty("deviceSeril")]
        public string DeviceSerial
        {
            get;
            set;
        }
   
        [JsonProperty("custominfoType")]
        public string CustominfoType
        {
            get;
            set;
        }
        [JsonProperty("custominfo")]
        public string Custominfo
        {
            get;
            set;
        }
    }
}
